
import copy
from lib.hw2.sorting import Sorting
import time
import matplotlib.pyplot as plt

class Test_UF(object):

    selection_time = []
    insertion_time = []
    shell_time = []
    merge_time = []
    heap_time = []
    quick_time = []
    sorted_time = []


    @classmethod
    def setup_class(klass):
        """This method is run once for each class before any tests are run"""
        print ("\n#####  Start Function Tests   #####\n")

    def test_one(self):
        pass

    def test_two(self):
        expected = (1, 2, 3)
        actual = (1, 2, 3)
        assert expected == actual

    def test_selection_sort_0(self):

        # initialize testbed
        for i in range (1,9):
            arr_under_test = Sorting()
            arr_under_test.sort_init(10*i*i)

            # test against built in python sorted() function.
            #expected = sorted(copy.deepcopy(arr_under_test.get_id()))
            expected = sorted(arr_under_test.get_id())
            start = time.clock()
            actual = arr_under_test.selection_sort()
            finish = time.clock()
            time_taken = finish - start
            Test_UF.selection_time.append(time_taken)

            assert expected == actual


    def test_insertion_sort_0(self):

       for i in range (1,9):
        # initialize testbed
            arr_under_test = Sorting()
            arr_under_test.sort_init(10*i)

            expected = sorted(arr_under_test.get_id())

            start = time.clock()
            actual = arr_under_test.insertion_sort()
            finish = time.clock()
            time_taken = finish - start
            Test_UF.insertion_time.append(time_taken)

            assert expected == actual

    def test_shell_sort_0(self):

        for i in range(1, 9):
        # initialize testbed
            arr_under_test = Sorting()
            arr_under_test.sort_init(10*i)

            expected = sorted(arr_under_test.get_id())

            start = time.clock()
            actual = arr_under_test.shell_sort()
            finish = time.clock()
            time_taken = finish - start
            Test_UF.shell_time.append(time_taken)
            assert expected == actual

    def test_heap_sort_0(self):
        for i in range(1, 9):
            # initialize testbed
            arr_under_test = Sorting()
            arr_under_test.sort_init(10*i)

            expected = sorted(arr_under_test.get_id())

            start = time.clock()
            actual = arr_under_test.heap_sort()
            finish = time.clock()
            time_taken = finish - start
            Test_UF.heap_time.append(time_taken)

        assert expected == actual

    def test_merge_sort_0(self):
        for i in range(1, 9):
            # initialize testbed
            arr_under_test = Sorting()
            arr_under_test.sort_init(10*i)

            expected = sorted(arr_under_test.get_id())

            start = time.clock()
            actual = arr_under_test.merge_sort(0, len(arr_under_test.get_id())-1)
            finish = time.clock()
            time_taken = finish - start
            Test_UF.merge_time.append(time_taken)

            assert expected == actual


    def test_quick_sort_0(self):

        for i in range(1, 9):
            # initialize testbed
            arr_under_test = Sorting()
            arr_under_test.sort_init(10*i)

            sorted_start = time.clock()
            expected = sorted(arr_under_test.get_id())
            sorted_finish = time.clock()
            sorted_final = sorted_finish - sorted_start
            Test_UF.sorted_time.append(sorted_final)


            start = time.clock()
            actual = arr_under_test.quick_sort()
            finish = time.clock()
            time_taken = finish - start
            Test_UF.quick_time.append(time_taken)

            assert expected == actual

        plt.plot([1, 2, 3, 4, 5, 6, 7, 8], Test_UF.selection_time, label='selection')
        plt.plot([1, 2, 3, 4, 5, 6, 7, 8], Test_UF.insertion_time, label='insertion')
        plt.plot([1, 2, 3, 4, 5, 6, 7, 8], Test_UF.shell_time, label='shell')
        plt.plot([1, 2, 3, 4, 5, 6, 7, 8], Test_UF.merge_time, label='merge')
        plt.plot([1, 2, 3, 4, 5, 6, 7, 8], Test_UF.heap_time, label='heap')
        plt.plot([1, 2, 3, 4, 5, 6, 7, 8], Test_UF.quick_time, label='quick')
        plt.plot([1, 2, 3, 4, 5, 6, 7, 8], Test_UF.sorted_time, label='sorted')
        plt.legend()

        plt.xscale('log')
        plt.yscale('log')
        plt.title('log')
        plt.ylabel('some numbers')
        plt.show()




