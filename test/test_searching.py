import copy
import random
import time
from lib.hw3.search_trees import Array_Search
from lib.hw3.search_trees import BST
from lib.hw3.search_trees import RBBST
import matplotlib.pyplot as plt


class Test_Search(object):

    sequential_search = []
    binary_search = []
    binary_search_tree = []
    red_black_binary_search_tree = []
    python_search = []


    @classmethod
    def setup_class(klass):
        """This method is run once for each class before any tests are run"""
        print("\n#####  Start Function Tests   #####\n")

    def test_one(self):
        pass

    def test_two(self):
        expected = (1, 2, 3)
        actual = (1, 2, 3)
        assert expected == actual

    def test_sequential_search_0(self):

        # initialize testbed
        for i in range (1,9):
            set_sz = 10 * i
            vals = random.sample(range(1, 100 * i ), set_sz)
            op_under_test = Array_Search(vals)

            # python search result
            key = vals[9]
            expected = vals.index(key)

            start = time.clock()
            actual = op_under_test.squential_search(key)
            finish = time.clock()
            time_taken = finish - start
            Test_Search.sequential_search.append(time_taken)

            assert expected == actual

    def test_bsearch_0(self):

        # initialize testbed
        for i in range(1,9):
            set_sz = 10 * i
            vals = random.sample(range(1, 100 * i), set_sz)
            vals = sorted(vals)
            op_under_test = Array_Search(vals)
            # python search result
            key = vals[9]
            start1 = time.clock()
            expected = vals.index(key)
            finish1 = time.clock()
            time_taken = finish1 - start1
            Test_Search.python_search.append(time_taken)

            start = time.clock()
            actual = op_under_test.bsearch(key)
            finish = time.clock()
            time_taken = finish - start
            Test_Search.binary_search.append(time_taken)

            assert expected == actual

    def test_bst_0(self):

        # initialize testbed
        for i in range(1,9):
            set_sz = 10 * i
            op_under_test = BST()

            vals = random.sample(range(1, 100 * i), set_sz)

            for idx in range(set_sz):
                op_under_test.insert(vals[idx])

            # python search result
            key = vals[9]
            expected = True

            start = time.clock()
            actual = op_under_test.bsearch(key)
            finish = time.clock()
            time_taken = finish - start
            Test_Search.binary_search_tree.append(time_taken)

            assert expected == actual

        # initialize testbed
        # op_under_test = BST()
        # op_under_test.init_bst(10)
        #
        # expected = sorted(op_under_test.get_id())
        #
        # actual = op_under_test.bsearch()
        #
        # assert expected == actual

    def test_rbbst_0(self):

        # initialize testbed
        for i in range(1,9):
            set_sz = 10*i
            op_under_test = RBBST()

            vals = random.sample(range(1, 100 * i), set_sz)

            for idx in range(set_sz):
                testing = vals[idx]
                op_under_test.insert(vals[idx])

            # python search result
            key = vals[9]
            expected = True
            start = time.clock()
            actual = op_under_test.bsearch(key)
            finish = time.clock()
            time_taken = finish - start
            Test_Search.red_black_binary_search_tree.append(time_taken)

            assert expected == actual

        plt.plot([1, 2, 3, 4, 5, 6, 7, 8], Test_Search.sequential_search, label='sequential')
        plt.plot([1, 2, 3, 4, 5, 6, 7, 8], Test_Search.binary_search, label='binary')
        plt.plot([1, 2, 3, 4, 5, 6, 7, 8], Test_Search.binary_search_tree, label='binary ST')
        plt.plot([1, 2, 3, 4, 5, 6, 7, 8], Test_Search.red_black_binary_search_tree, label='red-black')
        plt.plot([1, 2, 3, 4, 5, 6, 7, 8], Test_Search.python_search, label='python')

        plt.legend()

        plt.xscale('log')
        plt.yscale('log')
        plt.title('log')
        plt.ylabel('some numbers')
        plt.show()

        # initialize testbed
        # op_under_test = BST()
        # op_under_test.init_bst(10)
        #
        # expected = sorted(op_under_test.get_id())
        #
        # actual = op_under_test.bsearch()
        #
        # assert expected == actual



