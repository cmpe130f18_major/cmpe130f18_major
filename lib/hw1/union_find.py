

# 1. quick-find
# 2. quick-union
# 3. weighted QU
# 4. QU + path compression
# 5. weighted QU + path compression

class UF(object):
    """Union Find class

    """

    def __init__(self):
        self.id = []
        self.sz = []


    def qf_init(self, N):
        """initialize the data structure

        """
        for x in range (0, N-1):
            self.id.append(x)
        self.sz = [1]*N

    def qf_connected(self, p, q):
        """Find operation for Quick-Find Algorithm.
        simply test whether p and q are connected

        """

        return self.id[p] == self.id[q]


    def qf_union(self, p, q):
        """Union operation for Quick-Find Algorithm.

        connect p and q. You need to
        change all entries with id[p] to id[q]
        (linear number of array accesses)

        """
        pid = self.id[p]
        qid = self.id[q]
        for i in  range(0, len(self.id)):
            if self.id[i] == pid:
                self.id[i] = qid

        return True

    def qu_union(self, p, q):
        """Union operation for Quick-Union Algorithm.
         connect p and q.

         """
        i = UF.root(self, p)
        j = UF.root(self, q)
        self.id[i] = j

        return 0

    def root(self, i):
        while i != self.id[i]:
            i = self.id[i]
        return i

    def qu_connected(self, p, q):
        """Find operation for Quick-Union Algorithm.
         test whether p and q are connected

         """


        return UF.root(self, p) == UF.root(self, q)

    def wqu_union(self, p, q):
        """Union operation for Weighted Quick-Union Algorithm.
         connect p and q.

         """

        i = UF.root(self, p)
        j = UF.root(self, q)
        if self.sz[i] < self.sz[j]:
            self.id[i] = j
            self.sz[j] += self.sz[i]
        else:
            self.id[j] = i
            self.sz[i] += self.sz[j]

        return 0

    def wqu_connected(self, p, q):
        """Find operation for Weighted Quick-Union Algorithm.
         test whether p and q are connected

         """
        return UF.root(self, p) == UF.root(self, q)

    def pqu_union(self, p, q):
        """Union operation for path compressed Quick-Union Algorithm.
         connect p and q.

         """
        i = UF.patRoot(self, p)
        j = UF.patRoot(self, q)
        self.id[i] = j

        return 0

    def patRoot(self,i):
        while i != self.id[i]:
            self.id[i] = self.id[self.id[i]]
            i = self.id[i]
        return i

    def pqu_connected(self, p, q):
        """Find operation for path compressed Quick-Union Algorithm.
         test whether p and q are connected

         """

        return UF.root(self, p) == UF.root(self, q)

    def wpqu_union(self, p, q):
        """Union operation for Weighted path compressed Quick-Union Algorithm.
         connect p and q.

         """
        i = UF.patRoot(self, p)
        j = UF.patRoot(self, q)
        if self.sz[i] < self.sz[j]:
            self.id[i] = j
            self.sz[j] += self.sz[i]
        else:
            self.id[j] = i
            self.sz[i] += self.sz[j]

        return 0

    def wpqu_connected(self, p, q):
        """Find operation for Weighted path compressed Quick-Union Algorithm.
         test whether p and q are connected

         """

        return UF.root(self, p) == UF.root(self, q)