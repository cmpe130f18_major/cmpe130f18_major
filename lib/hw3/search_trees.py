import time
import random
import copy


class Array_Search:
    def __init__(self, array):
        self.array = array

    def init_array_search(self, val_array):
        self.array = Array_Search(val_array)

    def squential_search(self, key):

        idx = 0
        for num in self.array:
            if num == key:
                return idx
            idx = idx+1
        return idx

    def bsearch(self, val):
        low = 0
        high = len(self.array) - 1

        while low <= high:
            mid = round((high + low) / 2)
            if val > self.array[mid]:
                low = mid + 1
            elif val < self.array[mid]:
                high = mid - 1
            else:
                return mid


class BST_Node:
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None


class BST:
    def __init__(self):
        self.root = None

    def init_bst(self, val):
        self.root = BST_Node(val)

    def insert(self, val):
        if self.root is None:
            self.init_bst(val)
        else:
            self.insertNode(self.root, val)

    def insertNode(self, current, val):
        if val > current.val:
            if current.right is None:
                current.right = BST_Node(val)
            else:
                BST.insertNode(self, current.right, val)

        elif val < current.val:
            if current.left is None:
                current.left = BST_Node(val)
            else:
                BST.insertNode(self, current.left, val)


        return False

    def bsearch(self, val):
        if val == self.root.val:
            return True
        current = self.root

        while current is not None:
            if val < current.val:
                if current.left is None:
                    return False
                else:
                    current = current.left
            if val > current.val:
                if current.right is None:
                    return False
                else:
                    current = current.right
            if val == current.val:
                return True

        return False

    def searchNode(self, current, val):

        return False

    def delete(self, val):

        return False


class RBBST_Node:
    def __init__(self, val, color, parent):
        self.val = val
        self.left = None
        self.right = None
        self.color = color
        self.parent = parent

    def hasLeftChild(self):
        return self.left

    def hasRightChild(self):
        return self.right

    def isLeftChild(self):
        if self.parent and (self.parent.left == self):
            return True

    def isRightChild(self):
        return self.parent and self.parent.right == self


RED = True
BLACK = False


class RBBST:
    def __init__(self):
        self.root = None
        self.size = 0
        self.nil = None

    def init_rbbst(self, val, color, parent):
        self.root = RBBST_Node(val, color, parent)

    def is_red(self, current):
        if current is not None:
            if current.color is RED:
                return True

        return False

    def __copy(self, node):
        self.left = node.left
        self.right = node.right
        self.val = node.val
        self.color = node.color

    def rotate_left(self, current):
        temp = current.right
        current.right = temp.left
        if temp.left is not None:
            temp.left.parent = current
        temp.parent = current.parent
        if current.parent == self.nil:
            self.root = temp
        elif current.isLeftChild():
            current.parent.left = temp
        else:
            current.parent.right = temp
        temp.left = current
        current.parent = temp

    def rotate_right(self, current):
        temp = current.left
        current.left = temp.right
        if temp.right != self.nil:
            temp.right.parent = current
        temp.parent = current.parent
        if current.parent == self.nil:
            self.root = temp
        elif current.isRightChild():
            current.parent.right = temp
        else:
            current.parent.left = temp
        temp.right = current
        current.parent = temp

        return temp


    def flip_colors(self, current):
        # if RBBST.is_red(self, current) is False and RBBST.is_red(self, current.left) and RBBST.is_red(self, current.right):
        current.color = RED
        current.left.color = BLACK
        current.right.color = BLACK

        return self.root

    def insert(self, val):
        if self.root is None:
            self.init_rbbst(val, RED, None)
        else:
            self.insertNode(self.root, val)

    def insertNode(self, current, val):
        if val > current.val:
            if current.right is None:
                current.right = RBBST_Node(val, RED, current)
            else:
                RBBST.insertNode(self, current.right, val)

        elif val < current.val:
            if current.left is None:
                current.left = RBBST_Node(val, RED, current)
            else:
                RBBST.insertNode(self, current.left, val)

        if current is not None:
            if RBBST.is_red(self, current.right) and not RBBST.is_red(self, current.left):
                RBBST.rotate_left(self, current)
            if RBBST.is_red(self, current.left) and RBBST.is_red(self, current.left.left):
                RBBST.rotate_right(self, current)
            if RBBST.is_red(self, current.left) and RBBST.is_red(self, current.right):
                RBBST.flip_colors(self, current)

        return

    def bsearch(self, val):
        if val == self.root.val:
            return True
        current = self.root

        while current is not None:
            if val < current.val:
                if current.left is None:
                    return False
                else:
                    current = current.left
            if val > current.val:
                if current.right is None:
                    return False
                else:
                    current = current.right
            if val == current.val:
                return True

        return False

    def searchNode(self, current, val):

        return False


if __name__ == "__main__":

    # iteration
    set_szs = [10]
    timing = []