# 1. selection sort
# 2. insertion sort
# 3. shell sort
# 4. heap sort
# 5. merge sort
# 6. quick sort

import random


class Sorting(object):
    """Sorting class """

    def __init__(self):
        self.id = []
        self.elements = 0
        self.time_taken = 0

    def sort_init(self, N):
        """initialize the data structure"""
        try:
            self.id = random.sample(range(1, N ** 3), N)
        except ValueError:
            print('Sample size exceeded population size.')

        # self.id = [random.randint(0, N - 1) for i in range(N)]

    def get_id(self):
        """initialize the data structure"""
        return self.id

    def selection_sort(self):
        """Selection sort algorithm is an
        in-place comparison sort. It has O(n^2) time complexity, making it
        inefficient on large lists, and generally performs worse than the
        similar insertion sort """

        for i_idx, i_item in enumerate(self.id):
            minimum = i_idx

            for j_idx in range(i_idx+1, len(self.id)):

                if self.id[j_idx] < self.id[minimum]:
                    minimum = j_idx

            Sorting.swap(self, i_idx, minimum)

        return self.id

    def insertion_sort(self):
        """Insertion sort is a simple sorting algorithm that builds the final
        sorted array (or list) one item at a time. More efficient in practice
        than most other simple quadratic (i.e., O(n^2)) algorithms such as
        selection sort or bubble sort specifically an

        """
        for i in range(0, len(self.id)):
            j = i
            for j in range(j, 0, -1):
                if self.id[j] < self.id[j-1]:
                    Sorting.swap(self, j, j-1)
                else:
                    break

        return self.id

    def shell_sort(self):
        """Shell sort also known as  or Shell's method, is an in-place comparison sort.
        It can be seen as either a generalization of sorting by exchange (bubble sort)
        or sorting by insertion (insertion sort). """

        h = 1
        while h < len(self.id):
            h = 3*h + 1
        while h >= 1:
            i = h
            for i in range(i, len(self.id)):
                j = i
                while j >= h:
                    if self.id[j] < self.id[j-h]:
                        Sorting.swap(self, j, j-h)
                        j -= h
                    else:
                        break

            h = round(h/3)

        return self.id

    def heapify(self, n, i):
        largest = i
        left = 2 * i + 1
        right = 2 * i + 2

        if left < n and self.id[i] < self.id[left]:
            largest = left

        if right < n and self.id[largest] < self.id[right]:
            largest = right

        if largest != i:
            self.id[i], self.id[largest] = self.id[largest], self.id[i]

            # Heapify the root.
            Sorting.heapify(self, n, largest)

    def heap_sort(self):
        """Heapsort is an improved selection sort: it divides its input into a sorted
        and an unsorted region, and it iteratively shrinks the unsorted region by
        extracting the largest element and moving that to the sorted region.

        """
        n = len(self.id)

        for i in range(n, -1, -1):
            Sorting.heapify(self, n, i)

        for i in range(n - 1, 0, -1):
            Sorting.swap(self, i, 0)
            Sorting.heapify(self, i, 0)

        return self.id

    def merge(self, low, mid, hi):

        num1 = mid - low + 1
        num2 = hi - mid

        left_temp = [0] * num1
        right_temp = [0] * num2

        # Copy data to temp arrays L[] and R[]
        for first_sub in range(0, num1):
            left_temp[first_sub] = self.id[low + first_sub]

        for second_sub in range(0, num2):
            right_temp[second_sub] = self.id[mid + 1 + second_sub]

        first_sub = 0
        second_sub = 0
        merged = low

        while first_sub < num1 and second_sub < num2:
            if left_temp[first_sub] <= right_temp[second_sub]:
                self.id[merged] = left_temp[first_sub]
                first_sub += 1
            else:
                self.id[merged] = right_temp[second_sub]
                second_sub += 1
            merged += 1

        while first_sub < num1:
            self.id[merged] = left_temp[first_sub]
            first_sub += 1
            merged += 1

        while second_sub < num2:
            self.id[merged] = right_temp[second_sub]
            second_sub += 1
            merged += 1

    def merge_sort(self, low, hi):
        """Merge sort is a divide and conquer algorithm that was invented
        by John von Neumann in 1945. Most implementations produce a stable
        sort, which means that the implementation preserves the input order
        of equal elements in the sorted output.
        """
        if hi <= low:
            return
        mid = round((low + (hi - 1))/2)
        Sorting.merge_sort(self, low, mid)
        Sorting.merge_sort(self, mid+1, hi)
        Sorting.merge(self, low, mid, hi)
        return self.id

    def quick_sort(self):
        """Quicksort (sometimes called partition-exchange sort) is an efficient
        sorting algorithm. Developed by Tony Hoare in 1959. It is still a commonly
        used algorithm for sorting. When implemented well, it can be about two or
        three times faster than its main competitors, merge sort and heapsort.
        """
        random.shuffle(self.id)
        Sorting.sort(self, 0, len(self.id)-1)

        return self.id

    def partition(self, lo, hi):
        i = (lo - 1)
        pivot = self.id[hi]

        for j in range(lo, hi):
            if self.id[j] <= pivot:
                i = i + 1
                Sorting.swap(self, i, j)
        Sorting.swap(self, i+1, hi)
        return i+1

    def sort(self, lo, hi):
        if hi <= lo:
            return
        j = Sorting.partition(self, lo, hi)
        Sorting.sort(self, lo, j-1)
        Sorting.sort(self, j+1, hi)

    def swap(self, item1, item2):
        self.id[item1], self.id[item2] = self.id[item2], self.id[item1]

